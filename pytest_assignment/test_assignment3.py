import pytest


class TestClass:

    # Cognizant Method-1
    def test_cognizantone(self):
        print("This is Cognizant Method-1")

    # Cognizant Method-2
    def test_cognizanttwo(self):
        print("This is Cognizant Method-2")

    # Smoke Test-1
    @pytest.mark.smoke
    def test_randommethodone(self):
        print("This is a random method for smoke - First occurrence")

    # Skip Test
    @pytest.mark.skip
    def test_randommethodtwo(self):
        print("This is a random method - SKIPPING")

    # Smoke Test-2
    @pytest.mark.smoke
    def test_randommethodthree(self):
        print("This is a random method for smoke - Second occurrence")

    def test_randommethodfour(self):
        print("This is a random method")

    def test_randommethodfive(self):
        print("This is a random method")

    #Cognizant Method-3
    def test_cognizantthree(self):
        print("This is Cognizant Method-3")

    def test_randommethodsix(self):
        print("This is a random method")

    # XFail Method
    @pytest.mark.xfail
    def test_randommethodseven(self):
        print("This is a random method - XFAIL")

# -------------------Commands to run the tests as per the requirements-------------------------

# Execution 1
# pytest -vs

# Execution 2
# pytest -vs -m smoke

# Execution 3
# pytest -vs -k cognizant
